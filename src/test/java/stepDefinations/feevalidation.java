package stepDefinations;

import Resources.TestDataUtil;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import staticdata.feeband;

import java.util.ArrayList;
import java.util.List;


public class feevalidation {

    List<Double> volumelist = new ArrayList<>();
    List<Double> calculatedFee = new ArrayList<>();
    List<Double> feelist = new ArrayList<>();
    String sheetname= "Sheetname";
    int p= TestDataUtil.testData.getRowCount(sheetname);
    double sumofvolume = 0;
    double dailytrades=0;



    @When("I filter data using columns {string} as {string}, {string} as {string},{string} as{string}, {string} as {string}")
    public void iFilterDataUsingColumnsAsAsAsAs(String booked, String bv, String clientnr, String clv, String fee, String fv, String date, String dv) {
        for (int i=0 ;i < p;i++) {
            String b = TestDataUtil.testData.getCellData(sheetname, booked, i);
            String c = TestDataUtil.testData.getCellData(sheetname, clientnr, i);
            String f = TestDataUtil.testData.getCellData(sheetname, fee, i);
            String d = TestDataUtil.testData.getCellData(sheetname, date, i);
            if(b.equals(bv) & c.equals(clv) & f.equals(fv) & d.equals(dv)) {
                Double volumefee = Double.parseDouble(TestDataUtil.testData.getCellData(sheetname, "Volume", i));
                volumelist.add(volumefee);
                System.out.println("volumeeeeee: " + volumelist);

                Double fees = Double.parseDouble(TestDataUtil.testData.getCellData(sheetname, "Fee", i));
                feelist.add(fees);
                System.out.println("feeeeeee: " + feelist);
            }
        }
    }

    @Then("Calculated daily trades using DaysInMonth as {int}")
    public void calculatedDailyTradesUsingDaysInMonthAs(int arg0) {
        for(Double d : volumelist)
            sumofvolume += d;
        System.out.println("aaaaaa: " + sumofvolume);
        dailytrades=(sumofvolume/arg0);

    }


    @And("Identify Fee using Band and multiply with Monthly volume")
    public void identifyFeeUsingBandAndMultplyWithMonthlyVolume() {
        double d = feeband.getBandfee(dailytrades);
        System.out.println("zzzzzzzzzzzzz " + d );

        for (Double aDouble : volumelist) {
            double abbb = aDouble * d;
            calculatedFee.add(abbb);
        }

    }

    @Then("Validate calculated fee is same as provided fee.")
    public void validateCalculatedFeeIsSameAsProvidedFee() {
        Assert.assertEquals(calculatedFee, volumelist);
    }


//    @Given("I have excel file as input for fee validation")
//    public void iHaveExcelFileAsInputForFeeValidation() {
//
//        List<Double> values = new ArrayList<>();
//        String yn ;
//        String ar ;
//        int p= TestDataUtil.testData.getRowCount("AccountShipToCreate");
//        System.out.println("nr of rows" + p);
//        for (int i=0;i < p;i++)
//        {
//            yn = TestDataUtil.testData.getCellData("AccountShipToCreate", "ParentAccount", i);
//            ar = TestDataUtil.testData.getCellData("AccountShipToCreate", "AEResponsible", i);
//
//            if(yn.equals("Y") & ar.equals("ABC")) {
//                Double userName = Double.parseDouble(TestDataUtil.testData.getCellData("AccountShipToCreate", "ProductLine", i));
//                volumne.add(userName);
//            }
//        }
//        System.out.println("aa" + values);
//
//        // Sum all the elements of list
//        double sum = 0;
//        for(Double d : values)
//            sum += d;
//        System.out.println("aaaaaa: " + sum);
//
//
//        // Multiplying all the values of list
//        double[] input = {21.00, 0, 8.23, -12.11};
//        double[] result = DoubleStream.of(input).map(i -> i * 0.344).toArray();
//        System.out.println(Arrays.toString(result));
//
//
//        //boolean aaa = values.equals(values1);
//        //System.out.println("mmm" + aaa);
//
//    }

}
