package staticdata;

import java.util.Arrays;

public enum feeband {
    BAND1 (0, 2500),
    BAND2 (2501, 12500),
    BAND3 (12501, 4500),
    BAND4 (45001, 250000),
    BAND5 (250001, 400000),
    BAND6 (400001, 550000),
    BAND7 (550001, 1200000),
    BAND8 (1200001, 1500000),
    BAND9 (1500001, 999999999);

    private final double min;
    private final double max;

    feeband(double min, double max) {
        this.min = min;
        this.max = max;
    }

    public String getRangeAccount() {
        return String.format("%f,%f", min, max);
    }

    public static feeband get(double val) {
        return Arrays.stream(values())
                .filter(r -> val >= r.min && val <= r.max)
                .findFirst()
                .orElse(null);
    }
    public static double getBandfee(double value){
        double fee = 0;
        if (get(value).equals(BAND1)){
            fee=0.04;}
        if (get(value).equals(BAND2)){
            fee=0.0025;}
        if (get(value).equals(BAND3)) {
            fee = 0.0225;}
        if (get(value).equals(BAND4)) {
            fee = 0.015;}
        if (get(value).equals(BAND5)){
            fee=0.011;}
        if (get(value).equals(BAND6)){
            fee=0.0075;}
        if (get(value).equals(BAND7)){
            fee=0.005;}
        if (get(value).equals(BAND8)){
            fee=0.045;}
        if (get(value).equals(BAND9)) {
            fee = 0.0035;
        }
        return fee;
    }
}
