Feature: Validating Negative scenarios for Beneficiary

  Scenario: Validate MRF Fee for clients
   #Given I have excel file as input for fee validation
    When I filter data using columns "Booked" as "Y", "ClientNumber" as "1600","FeeType" as"ABC", "Date" as "122022"
    Then Calculated daily trades using DaysInMonth as 23
    And Identify Fee using Band and multiply with Monthly volume
    Then Validate calculated fee is same as provided fee.