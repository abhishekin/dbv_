package cucumber.Options;


import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;


@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/features/", plugin = {
        "pretty",
        "json:target/cucumber.json", "html:target/default-html-reports", "rerun:target/rerun.txt"},
		glue= {"stepDefinations"})
public class TestRunner {

}
